#!/usr/bin/env bash
build () {
    docker build . -t sandbox-ansible:latest --build-arg ANSIBLE_RELEASE=${ANSIBLE_VERSION}
    if [[ "$1" = "-m" ]] ; then
            echo "multi-boxes setup..."
            docker build . -f docker/multibox.dockerfile -t sandbox-multi:latest
    fi
}
alias br='build && run'
run () {
    PLAYBOOKS=$(_mountAllFiles "/playbooks" "")
    DEV_HOSTVARS=$(_mountAllDirs "/inventories/dev/host_vars" "/hosts/host_vars")
    DEV_GROUPVARS=$(_mountAllDirs "/inventories/dev/group_vars" "/hosts/group_vars")
    STAGE_HOSTVARS=$(_mountAllDirs "/inventories/stage/host_vars" "/hosts/host_vars")

    if [[ "$1" = "-m" ]] ; then
        IMAGE_NAME=sandbox-multi:latest
    elif [[ -z "$1" ]] ; then
        IMAGE_NAME=sandbox-ansible:latest
    else
        IMAGE_NAME=$1
    fi

    docker run -it -p 5005:5005/tcp -p 5005:5005/udp -p 8153:8153/tcp -p 8153:8153/udp -p 3000:3000/tcp -p 3000:3000/udp\
        -v /vagrant/library:/etc/ansible/library \
        -v /vagrant/callback_plugins:/etc/ansible/callback_plugins \
        -v /vagrant/templates:/etc/ansible/templates \
        -v /vagrant/files:/etc/ansible/files \
        -v /vagrant/docker/ansible.cfg:/etc/ansible/ansible.cfg \
        -v /vagrant/inventories/dev/hosts.yml:/etc/ansible/hosts/dev/hosts.yml \
        -v /vagrant/inventories/stage/hosts.yml:/etc/ansible/hosts/stage/hosts.yml \
        $PLAYBOOKS $DEV_HOSTVARS $DEV_GROUPVARS $STAGE_HOSTVARS\
        ${IMAGE_NAME}
}

_mountAllFiles() {
   cd /vagrant$1 && for f in *.yml; do echo -n "-v /vagrant$1/$f:/etc/ansible$2/$f "; done
}
_mountAllDirs() {
   cd /vagrant$1 && for d in */; do echo -n "-v /vagrant$1/$d:/etc/ansible$2/$d "; done
}
