(
echo n # Add a new partition
echo p # Primary partition
echo 3 # Partition number
echo   # First sector (Accept default: 1)
echo   # Last sector (Accept default: varies)
echo t # Change new partition ID
echo 3 # Select the new partition
echo 8e # Change partition type to Linux LVM
echo w # Write changes
) | fdisk /dev/sda