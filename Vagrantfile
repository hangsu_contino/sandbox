# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'
settings = YAML.load_file './vagrant/vagrant.yml'

Vagrant.configure(2) do |config|
  config.vm.box = settings['vm']['box']
  config.vm.box_check_update = false
  config.vm.network "private_network", ip: settings['vm']['ip']
  config.vm.network "forwarded_port", guest: 2375, host: 2375
  config.vm.network "forwarded_port", guest: 5005, host: 5005
  config.vm.network "forwarded_port", guest: 8153, host: 8153
  config.vm.network "forwarded_port", guest: 3000, host: 3000

  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.name = settings['vm']['name']
    vb.memory = settings['vm']['memory']
    vb.customize ["modifyvm", :id, "--ioapic", "on", "--vram", "256", "--clipboard", "bidirectional"]
    vb.cpus = settings['vm']['cpu']
  end

  config.ssh.username = 'root'
  config.ssh.password = 'vagrant'
  config.ssh.insert_key = 'true'
  config.ssh.keep_alive = 'true'

  config.vm.provision "install-docker", type: "shell", inline: <<-SHELL
      yum remove docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-selinux \
        docker-engine-selinux \
        docker-engine
      yum install -y yum-utils \
        device-mapper-persistent-data \
        lvm2 \
        dos2unix
        vim
      yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo
      yum-config-manager --enable docker-ce-edge
      yum install -y docker-ce
  SHELL

  config.vm.provision "config-docker", type: "shell", inline: <<-SHELL
      mkdir -p /etc/systemd/system/docker.service.d
      echo '[Service]' > /etc/systemd/system/docker.service.d/docker.conf
      echo 'ExecStart=' >> /etc/systemd/system/docker.service.d/docker.conf
      echo 'ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock' >> /etc/systemd/system/docker.service.d/docker.conf
      systemctl enable docker
      systemctl daemon-reload
      systemctl restart docker
  SHELL

  config.vm.provision "docker-helloworld", type: "shell", inline: <<-SHELL
    docker run hello-world
  SHELL
  config.vm.provision "vagrantrc", type: "file", source: "vagrant/vagrantrc.sh", destination: "/root/.vagrantrc"
  config.vm.provision "vagrantrc-update", type: "shell", inline: <<-SHELL
    dos2unix /root/.vagrantrc
    echo "export ANSIBLE_VERSION=#{settings['dependency']['ansible_version']}" >> ~/.vagrantrc
    echo "source ~/.vagrantrc" >> ~/.bashrc
    echo "cd /vagrant" >> ~/.bashrc
  SHELL
  if settings['vm']['resize']
    config.vm.provision "dos2unix-files", type: "shell", inline: <<-SHELL
      cp /vagrant/vagrant/dangerous.sh ~/dangerous.sh
      chmod +x ~/dangerous.sh
      dos2unix ~/dangerous.sh
      cp /vagrant/vagrant/extenddisk.sh ~/extenddisk.sh
      chmod +x ~/extenddisk.sh
      dos2unix ~/extenddisk.sh
    SHELL
    config.vm.provision "remove-files", type: "shell", inline: <<-SHELL
      rm ~/dangerous.sh
      rm ~/extenddisk.sh
    SHELL
    config.vm.provision "fdisk", type: "shell", inline: <<-SHELL
      if [ -f ~/dangerous.sh ]; then ~/dangerous.sh; fi;
    SHELL
    config.vm.provision "extenddisk", type: "shell", inline: <<-SHELL
      if [ -f ~/extenddisk.sh ]; then ~/extenddisk.sh; fi;
    SHELL

  end
end