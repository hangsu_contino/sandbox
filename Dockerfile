#Debian 9 stretch
ARG ANSIBLE_RELEASE=2.6.2

FROM openjdk:8u162-jdk
ARG ANSIBLE_RELEASE=2.6.2-1ppa~trusty
RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list \
 && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367 \
 && apt-get update \
 && apt-get install -y ansible=${ANSIBLE_RELEASE}-1ppa~trusty vim ssh apt-transport-https\
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config

COPY docker/id_rsa_pi /etc/ansible/keys/secrete
RUN chmod 600 /etc/ansible/keys/secrete

COPY docker/ansible.cfg /etc/ansible/ansible.cfg
COPY callback_plugins /etc/ansible/callback_plugins
COPY playbooks/*.yml /etc/ansible/
COPY files /etc/ansible/files

RUN rm -rf /etc/ansible/hosts
COPY inventories/*/host_vars /etc/ansible/hosts/host_vars/
COPY inventories/*/group_vars /etc/ansible/hosts/group_vars/
COPY inventories/dev/hosts.yml /etc/ansible/hosts/dev/hosts.yml
COPY inventories/stage/hosts.yml /etc/ansible/hosts/stage/hosts.yml

RUN echo "alias av='ansible -m debug -a \"var=hostvars[inventory_hostname]\"'" >> ~/.bashrc
RUN echo "alias ap='ansible-playbook'" >> ~/.bashrc
RUN echo "playvar () { ansible-playbook \$2 -e \"target_host='\$1'\"; }" >> ~/.bashrc
RUN echo "cd /etc/ansible" >> ~/.bashrc

ENTRYPOINT service ssh restart && echo "Welcome to Sandbox Image" && bash
