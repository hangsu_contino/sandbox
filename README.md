
## Setup
  * Install latest version of [Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads)
  * Install [Vagrant](https://www.vagrantup.com/downloads.html)
  * Clone this repo to your working dir
  * Replace tokens in  _vagrant/vagrant.yml_ with real values
  This is the recommended approach to prevent accidental check in of your precious easy-to-guess same-for-all-site 
  credentials in _vagrant/vagrant.yml_
  * Copy private key ansible user to _docker/id_rsa_
  * Run `vagrant up` in your cloned repo. This will bring up a vagrant CentOS 7.4 box on Windows machine, the vagrant box can be configured through a yaml file.
  * Run `vagrant ssh` login to the vm, or use _root:vagrant_ to log onto the vm
  * Use alias 'build'(`docker build . -t sandbox-ansible`) to build docker image from _Dockerfile_. This will provision a docker image for Ansible playbooks on the linux vagrant box
  * Use alias 'run'(`docker run -it sandbox-ansible`) to run the built docker image

**NOTE**: The local image has more convenient setup to facilitate local dev and testing.
The folder structure is different from CI setup. See details in [Dockerfile](Dockerfile) and [vagratrc.sh](vagrant/vagrantrc.sh) <br>

**NOTE**: The run method mount files into docker image, so they will be updated without re-build the image.
If you can using _docker run_ to start the image, you will need to _docker build_ image everytime a file is changed.<br>

### Authentication
This playbook use ssh keys to authenticate with remote hosts. For dev/sandbox local connection type is used and 
No authentication is required.

#### local dev environment
Private key is expected at docker/id_rsa. <br>

**The private key need to be setup locally and should not be added into repo.**

## Increase Vagrant Disk Size
To increase the vagrant box max disk size, one needs to halt and destroy current vagrant box.
Then clear out the centos  VirtualBoxVMs folder, example _C:\VirtualBoxVMs\centos-7.4-x86_64_. So the newly created VM
will have name centos-7.4-x86_64-disk001.vmdk.<br>
This can also be done in VirtualBox GUI Files, Virtual Media Manager, Hard disks, remove all centos-7.4-x86_64-disk001.vmdk and alike.<br>
Change the vm.resize setting in _vagrant/vagrant.yml_ to _true_<br>
Then use _rv100.bat_ to start the process<br>
Vagrant box will be restarted few times during the process.
