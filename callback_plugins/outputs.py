from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
    callback: outputs
    type: stdout
    short_description: formated stdout/stderr display even task is successful
    description:
      - use this callback plugin to always display STDOUT, STDERR and MSG returned even when the task is successful
'''

from ansible.plugins.callback.default import CallbackModule as CallbackModule_default
from ansible import constants as C


class CallbackModule(CallbackModule_default):
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'outputs'

    def gather_outputs(self, result):
        save = {}
        for key in ['stdout', 'stdout_lines', 'stderr', 'stderr_lines', 'msg', 'logs', 'changes', 'module_stdout', 'module_stderr']:
            if key in result._result:
                save[key] = result._result.pop(key)
        return (save, result)

    def display(self, key, line, color):
        if line.strip():
            self._display.display("[%s]: %s" % (key, line), color)

    def spit_outputs(self, save):
        for key in ['msg']:
            if key in save:
                self.display(key, save[key], C.COLOR_HIGHLIGHT)
        for key in ['logs', 'changes']:
            if key in save:
                for line in save[key]:
                    self.display(key, line, C.COLOR_HIGHLIGHT)
        for key in ['stdout', 'module_stdout']:
            if key in save:
                self.display(key, save[key], C.COLOR_OK)
        for key in ['stdout_lines']:
            if key in save:
                for line in save[key]:
                    self.display(key, line, C.COLOR_OK)
        for key in ['stderr', 'module_stderr']:
            if key in save:
                self.display(key, save[key], C.COLOR_ERROR)
        for key in ['stderr_lines']:
            if key in save:
                for line in save[key]:
                        self.display(key, line, C.COLOR_ERROR)

    def v2_runner_on_ok(self, result):
        save, trimed_result = self.gather_outputs(result)
        CallbackModule_default.v2_runner_on_ok(self, trimed_result)
        self.spit_outputs(save)

    def v2_runner_on_failed(self, result, ignore_errors=False):
        save, trimed_result = self.gather_outputs(result)
        CallbackModule_default.v2_runner_on_failed(self, trimed_result, ignore_errors)
        self.spit_outputs(save)
